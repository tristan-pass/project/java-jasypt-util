package com.tristan.util.helper4password;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Helper4passwordApplication {

    public static void main(String[] args) {
        SpringApplication.run(Helper4passwordApplication.class, args);
        try{
            PwdDecOrEnc.run();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
