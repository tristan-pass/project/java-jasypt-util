
package com.tristan.util.helper4password;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.Scanner;

public class PwdDecOrEnc {
    static String secret = "";
//    static String secret = "l&id81!lw";
    static String ENCRYPTNAME = "encrypt";
    static String DECRYPTNAME = "decrypt";
    static String[] dirMeaningNameArr = {"加密", "解密"};
    static String oldTextSpecStart = "ENC(";
    static String oldTextSpecEnd = ")";

    /**
     * 初始化加密
     *
     * @return
     */
    public static StandardPBEStringEncryptor initEncryptor() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();
        config.setAlgorithm("PBEWithMD5AndDES");  // 加密类型

        // 输入密钥
        if (StringUtils.isEmpty(secret)) {
            System.out.println("--------------->请输入密钥------->");
            Scanner input = new Scanner(System.in);
            if (StringUtils.isEmpty(input)) {
                System.out.println("--------------->密钥不能为空------->");
                return null;
            }
            secret = input.next();
        }
        config.setPassword(secret);  // 密钥
        encryptor.setConfig(config);
        return encryptor;
    }

    /**
     * 删除原来的加密/解密文件夹
     *
     * @return
     */
    public static boolean deleteOldDir() {
        String[] dirNameArr = {"./" + ENCRYPTNAME, "./" + DECRYPTNAME};
        for (int i = 0; i < dirNameArr.length; i++) {
            String dirName = dirNameArr[i];
            String dirMeaningName = dirMeaningNameArr[i];
            File encryptDir = new File(dirName);
            if (encryptDir.exists() && encryptDir.isDirectory()) {
                System.out.println("清空原" + dirMeaningName + "文件夹");
                File[] files = encryptDir.listFiles();
                for (File file : files) {
                    if (file.exists()) file.delete();
                }
            } else {
                boolean isMkdirEncryptDir = encryptDir.mkdirs();
                if (!isMkdirEncryptDir) {
                    System.out.println("创建" + dirMeaningName + "文件夹失败");
                    return false;
                }
            }

        }
        return true;
    }

    /**
     * 生成加密文件和解密文件
     *
     * @return
     */
    public static boolean genFile(StandardPBEStringEncryptor encryptor) throws Exception {
        // 遍历所有文件
        File curDir = new File("./");
        File[] files = curDir.listFiles();
        for (File propertiesFile : files) {

            String curFileName = propertiesFile.getName();
            if (!curFileName.endsWith(".properties")) continue;
            System.out.println("正在解析文件: " + curFileName);

            // 加密文件
            String newEncryptFileName = "./" + ENCRYPTNAME + "/" + curFileName;
            File encryptFile = new File(newEncryptFileName);
            boolean isCreateEncryptFile = encryptFile.createNewFile();
            if (!isCreateEncryptFile) {
                System.out.println("创建加密文件失败(" + newEncryptFileName + ")");
                return false;
            }
            BufferedWriter encryptWriter = new BufferedWriter(new FileWriter(encryptFile));
            // 解密文件
            String newDncryptFileName = "./" + DECRYPTNAME + "/" + curFileName;
            File dncryptFile = new File(newDncryptFileName);
            boolean isCreateDncryptFile = dncryptFile.createNewFile();
            if (!isCreateDncryptFile) {
                System.out.println("创建解密文件失败(" + newDncryptFileName + ")");
                return false;
            }
            BufferedWriter dncryptWriter = new BufferedWriter(new FileWriter(dncryptFile));

            // 解析properties文件
            // 遍历所有行
            BufferedReader bufferedReader = new BufferedReader(new FileReader(propertiesFile));
            String line;
            boolean isFirst = true;
            while ((line = bufferedReader.readLine()) != null) {
                if (!isFirst) {
                    encryptWriter.write("\n");
                    dncryptWriter.write("\n");
                }
                isFirst = false;
                int splitIndex = line.indexOf("=");
                if (splitIndex < 0) {
                    encryptWriter.write(line);
                    dncryptWriter.write(line);
                    continue;
                }
                String prefixText = line.substring(0, splitIndex + 1);
                String oldText = line.substring(splitIndex + 1);
                System.out.println("初始值为:>>>" + oldText + "<<<");


                // 生成加密
                String encryptText = encryptor.encrypt(oldText);
                encryptWriter.write(prefixText + oldTextSpecStart + encryptText + oldTextSpecEnd);
                // 生成解密
                String decryptText = oldText;
                int oldTextSpecStartIndex = oldText.indexOf(oldTextSpecStart);
                int oldTextSpecEndIndex = oldText.indexOf(oldTextSpecEnd);
                if (oldTextSpecStartIndex == 0 && oldTextSpecEndIndex > 0) {
                    try{
                        decryptText = encryptor.decrypt(oldText.substring(oldTextSpecStartIndex + oldTextSpecStart.length(), oldTextSpecEndIndex));
                    }catch (Exception e){
                        System.out.println(">>>>>>>>>>>>>>>解密失败");
                    }
                }
                dncryptWriter.write(prefixText + decryptText);
            }
            encryptWriter.flush();
            dncryptWriter.flush();
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        }
        return true;
    }

    /**
     * 需求:
     * 对当前文件夹下的所有properties文件进行解析
     * 对每行的value进行加密
     * 将该值存放到 encrypt/原文件名+.encrypt 文件 中
     * <p>
     * 对每行的value进行解密
     * 将该值存放到 decrypt/原文件名+.decrypt 文件 中
     */
    public static void run() throws Exception {
        System.out.println("当前运行路径为:" + new File("./").getCanonicalPath());

        // 初始化加密
        StandardPBEStringEncryptor encryptor = initEncryptor();
        if (encryptor == null) return;

        // 删除原来的加密/解密文件夹
        boolean isDeleteOldDir = deleteOldDir();
        if (!isDeleteOldDir) return;

        // 生成文件
        boolean isGenFile = genFile(encryptor);
        if (!isGenFile) return;
    }
}